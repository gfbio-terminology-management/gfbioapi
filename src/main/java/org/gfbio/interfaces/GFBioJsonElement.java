package org.gfbio.interfaces;

public abstract interface GFBioJsonElement
{
  public abstract boolean isArray();
}
