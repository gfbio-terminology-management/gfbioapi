package org.gfbio.interfaces;

import java.util.List;
import java.util.Map;

import org.gfbio.resultset.AllBroaderResultSetEntry;
import org.gfbio.resultset.HierarchyResultSetEntry;
import org.gfbio.resultset.MetadataResultSetEntry;
import org.gfbio.resultset.GFBioResultSet;
import org.gfbio.resultset.SearchResultSetEntry;
import org.gfbio.resultset.SynonymsResultSetEntry;
import org.gfbio.resultset.TermCombinedResultSetEntry;
import org.gfbio.resultset.TermOriginalResultSetEntry;
import org.gfbio.resultset.CapabilitiesResultSetEntry;

public abstract interface WSInterface
{
  public abstract String getAcronym();
  
  public abstract String getDescription();
  
  public abstract String getName();
  
  public abstract String getURI();
  
  public abstract List<String> getDomains();
  
  public abstract String getCurrentVersion();
  
  public abstract GFBioResultSet<SearchResultSetEntry> search(String paramString1, String paramString2);
  
  public abstract GFBioResultSet<TermOriginalResultSetEntry> getTermInfosOriginal(String paramString);
  
  public abstract GFBioResultSet<SearchResultSetEntry> getTermInfosProcessed(String paramString);
  
  public abstract GFBioResultSet<TermCombinedResultSetEntry> getTermInfosCombined(String paramString);
  
  public abstract GFBioResultSet<HierarchyResultSetEntry> getHierarchy(String paramString);
  
  public abstract GFBioResultSet<AllBroaderResultSetEntry> getAllBroader(String paramString);
  
  public abstract GFBioResultSet<SynonymsResultSetEntry> getSynonyms(String paramString);
  
  public abstract GFBioResultSet<MetadataResultSetEntry> getMetadata();
  
  public abstract GFBioResultSet<CapabilitiesResultSetEntry> getCapabilities();
  
  public abstract boolean supportsMatchType(String paramString);
  
  public abstract boolean isResponding();
  
  public static enum SearchTypes
  {
    exact,  included,  regex,  fuzzy;
    
    private SearchTypes() {}
  }
  
  public static enum SearchModes
  {
    exact,  included,  regex,  synonyms, abbreviations, commonNames;
    
    private SearchModes() {}
  }
  
  public static enum Services
  {
    allterms,  term,  synonyms,  metrics, metadata, search, suggest, narrower, allnarrower, broader, allbroader, hierarchy;
    
    private Services() {}
  }
  
  public static enum Domains
  {
    Biology("http://terminologies.gfbio.org/terms/ontology/Biology"),  Chemistry("http://terminologies.gfbio.org/terms/ontology/Chemistry"),  GeoSciences("http://terminologies.gfbio.org/terms/ontology/Geo-Sciences"),  Ecology("http://terminologies.gfbio.org/terms/ontology/Ecology"),  Botany("http://terminologies.gfbio.org/terms/ontology/Botany"),  Microbiology("http://terminologies.gfbio.org/terms/ontology/Microbiology"),  Mycology("http://terminologies.gfbio.org/terms/ontology/Mycology"),  Zoology("http://terminologies.gfbio.org/terms/ontology/Zoology");
    
    private String uri;
    
    private Domains(String s)
    {
      this.uri = s;
    }
    
    public String getUri()
    {
      return this.uri;
    }
  }
}
