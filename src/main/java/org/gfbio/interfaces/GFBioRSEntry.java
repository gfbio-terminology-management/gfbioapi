package org.gfbio.interfaces;

import org.gfbio.formating.GFBioResultSetEntry;

public abstract interface GFBioRSEntry
{
  public abstract GFBioResultSetEntry getEntry();
  
  public abstract boolean isOriginal();
}
