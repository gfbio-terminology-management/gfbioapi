/**
 * 
 */
package org.gfbio.util;

import java.io.IOException;
import java.io.InputStream;
import org.apache.log4j.Logger;
import org.gfbio.config.BaseConfig;
import org.gfbio.config.WSConfiguration;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;


public class YAMLConfigReader {

  private static final Logger LOGGER = Logger.getLogger(YAMLConfigReader.class);

  private static final String filename = "webservices.yaml";

  private static YAMLConfigReader instance;

  private BaseConfig config;

  private YAMLConfigReader() {

    LOGGER.debug("Reading YAML configuration from " + filename);

    try {
      // Loading the YAML file from the /resources folder
      ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
      InputStream in = classLoader.getResourceAsStream(filename);

      // Instantiating a new ObjectMapper as a YAMLFactory
      ObjectMapper om = new ObjectMapper(new YAMLFactory());

      // read in YAML entries as Java class
      config = om.readValue(in, BaseConfig.class);

    } catch (IOException e) {
      e.printStackTrace();
      LOGGER.error("Exception while reading YAML configuration " + e.getMessage());
    }

  }

  public static synchronized YAMLConfigReader getInstance() {
    if (instance == null)
      instance = new YAMLConfigReader();

    return instance;
  }

  public WSConfiguration getWSConfig(String acronym) {

    return config != null ? config.getConfigByAcronym(acronym) : null;
  }

}
