/**
 * 
 */
package org.gfbio.config;


public class COLConfig extends WSConfiguration {

  public COLConfig() {}

  public COLConfig(String contact, String name, String keywords, String description, String storage,
      String contribution, String contributors, String languages, String creators, String acronym,
      String webserviceURL, String uri, String version) {
    super(contact, name, keywords, description, storage, contribution, contributors, languages,
        creators, acronym, webserviceURL, uri, version);
  }

}
