package org.gfbio.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.gfbio.db.VirtGraphSingleton;
import org.ini4j.Wini;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

/**
 * Accessing the watcher.ini and providing its contents as gettable resources
 */
public class ConfigSvc {

  private static ConfigSvc instance;

  private static final String GRAPHURI = "http://terminologies.gfbio.org/terms/";

  private static final Logger LOGGER = Logger.getLogger(ConfigSvc.class);

  private String iniName = "/var/opt/ts/watcher.ini";

  private Wini watcherIni;

  // Virtuoso parameters
  private String hostURL, username, password;

  private String workDir;

  private boolean execSPARQL, uploadOnly;

  // Ontology specific properties
  private String ontologies_path;
  private String results_path;
  private String acronym;
  private String ontoFilename; // must not be the acronym
  private String graphUri, metadataUri;
  private boolean calcComplexMeetrics;

  private HashMap<String, String> namespaces = new HashMap<String, String>();

  public static ConfigSvc getInstance(String pathToIni) {
    if (ConfigSvc.instance == null) {
      ConfigSvc.instance = new ConfigSvc(pathToIni);
    }
    return ConfigSvc.instance;
  }

  private ConfigSvc(String pathToIni) {
    if (pathToIni != null)
      this.iniName = pathToIni;

    openINIFile();
    readNamespaces(new File(this.workDir + "namespaces.csv"));
  }

  /**
   * reads some initial application properties; these are general settings, which are used
   * throughout multiple applications
   */
  private void openINIFile() {

    LOGGER.info("loading ini file " + iniName);

    try {
      watcherIni = new Wini(new File(iniName));

      // general settings
      workDir = watcherIni.get("GENERAL", "workDir");
      ontologies_path = workDir + "ontologies/";
      uploadOnly = Boolean.valueOf(watcherIni.get("UPDATE", "uploadOnly"));
      execSPARQL = Boolean.valueOf(watcherIni.get("UPDATE", "execSPARQL"));
      hostURL = getSettingFromINI("VIRTUOSO", "virtuosoURL");
      username = getSettingFromINI("VIRTUOSO", "username");
      password = getSettingFromINI("VIRTUOSO", "password");
      graphUri = getSettingFromINI("VIRTUOSO", "GRAPHURI");
      metadataUri = getSettingFromINI("VIRTUOSO", "METADATAURI");

    } catch (IOException e) {
      LOGGER.warn("watcher.ini could not be found under /var/opt/ts");
      LOGGER.warn("Please make sure this file exists!");
      LOGGER.error(e.getLocalizedMessage());
    }
  }

  /**
   * 
   * 
   * @param acronym Terminology acronym, e.g., ENVO
   * @param key Property to query, e.g., description
   * @return Parameter value from graph {@link http://terminologies.gfbio.org/terms/Parameters}
   */
  public List<String> getParameterFromGraph(String acronym, String key) {

    // some properties might have more than one entry in the graph
    List<String> parameters = new ArrayList<String>();

    int acronymHash = acronym.toUpperCase().hashCode();

    String paramQuery = "select ?p ?param FROM <" + graphUri + "Parameters> WHERE {<" + GRAPHURI
        + acronymHash + "> ?p ?param. FILTER regex(?p, \"" + key + "\", \"i\")}";

    LOGGER.info(paramQuery);

    VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(paramQuery, set);
    vqe.setTimeout(1);
    vqe.execSelect();

    try {
      ResultSet rs = vqe.execSelect();

      while (rs.hasNext()) {
        QuerySolution qs = rs.next();
        if (qs.get("param").isLiteral())
          parameters.add(qs.get("param").asLiteral().getValue().toString());
        else if (qs.get("param").isResource())
          parameters.add(qs.get("param").asResource().toString());
      }
    } catch (Exception e) {
      LOGGER.error(e);
    }
    vqe.close();
    if (parameters.size() == 0) {
      LOGGER.warn("no value for parameter " + key + " was found! Please check the " + graphUri
          + "Parameters graph");
      return null;
      // throw new NotFoundException("Please check if the " + GRAPHURI + "Parameters graph exists");
    }

    return parameters;
  }

  /**
   * 
   * @param table Name of collection
   * @param key Filter key, e.g., 'name'
   * @param value Filter value
   * @param property Actual property to read, e.g., 'name' or 'location'
   * @return
   */
  public Object getSettingFromDB(String table, String key, String value, String property) {
    // return mSvc.readOne(table, key, value, property);

    return getSettingFromINI(value, property);
  }

  /**
   * Fallback method for {@link getSettingFromDB}
   * 
   * @param cat
   * @param key
   * @return
   */
  public String getSettingFromINI(String cat, String key) {
    return watcherIni.get(cat, key);
  }

  public List<String> getSettingsFromDB(String table, String key) {
    // return mSvc.readMany(table, key);

    return getSettingsFromINI(key);
  }

  /**
   * Fallback method for {@link getSettingsFromDB}
   * 
   * @param cat
   * @param key
   * @return
   */
  public List<String> getSettingsFromINI(String cat) {

    return watcherIni.getAll(cat).stream().map(section -> new String(section.toString()))
        .collect(Collectors.toList());
  }

  public String resolveNamespaceToUri(String property) {

    if (property.contains("http") || property.contains("https"))
      return property;

    String namespace = property.split(":")[0];
    String attribute = property.split(":")[1];

    return namespaces.get(namespace) + attribute;
  }

  private HashMap<String, String> readNamespaces(File test2) {

    try (BufferedReader TSVReader = new BufferedReader(new FileReader(test2))) {
      String line = null;
      while ((line = TSVReader.readLine()) != null) {
        String[] lineItems = line.split(",");
        namespaces.put(lineItems[0].trim(), lineItems[1].trim());
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return namespaces;
  }

  public String formatDuration(Duration duration) {
    long seconds = duration.getSeconds();
    long absSeconds = Math.abs(seconds);
    String positive =
        String.format("%d:%02d:%02d", absSeconds / 3600, (absSeconds % 3600) / 60, absSeconds % 60);
    return seconds < 0 ? "-" + positive : positive;
  }

  /**
   * 
   * @return e.g. /var/opt/ts/ontologies/envo/
   */
  public String getResultsPath() {
    return results_path;
  }

  public String getAcronym() {
    return acronym;
  }

  /**
   * 
   * @return e.g. /var/opt/ts/
   */
  public String getWorkDir() {
    return workDir;
  }

  /**
   * 
   * @return e.g. /var/opt/ts/ontologies/
   */
  public String getOntoDir() {
    return ontologies_path;
  }

  public String getOntoFilename() {
    return ontoFilename;
  }

  public boolean calcComplexMeetrics() {
    return calcComplexMeetrics;
  }

  public boolean isExecSPARQL() {
    return execSPARQL;
  }

  public boolean isUploadOnly() {
    return uploadOnly;
  }

  public String getHostURL() {
    return hostURL;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public String getGraphUri() {
    return graphUri;
  }

  public void setGraphUri(String graphUri) {
    this.graphUri = graphUri;
  }

  public String getMetadataUri() {
    return metadataUri;
  }

  public void setMetadataUri(String metadataUri) {
    this.metadataUri = metadataUri;
  }
}
