/**
 * 
 */
package org.gfbio.config;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * Administrates all available webservice configurations and allows access
 *
 */
public class BaseConfig {

  // base properties
  private String version;
  private String released;

  // available/configured webservices
  private COLConfig COLConfig;
  private PNUConfig PNUConfig;
  // private GEONAMESConfig GEONAMESConfig;
  private PESIConfig PESIConfig;
  private WORMSConfig WORMSConfig;
  // private ITISConfig ITISConfig;
  private DWBWSConfig DWBWSConfig;

  private Map<String, WSConfiguration> wsConfigs = new HashMap<String, WSConfiguration>();

  public WSConfiguration getConfigByAcronym(String acronym) {
    return wsConfigs.get(acronym);
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getReleased() {
    return released;
  }

  public void setReleased(String released) {
    this.released = released;
  }

  public COLConfig getCOLConfig() {
    return COLConfig;
  }

  public void setCOLConfig(COLConfig colConfig) {
    this.COLConfig = colConfig;
    wsConfigs.put(colConfig.getAcronym(), colConfig);
  }

  public PNUConfig getPNUConfig() {
    return PNUConfig;
  }

  public void setPNUConfig(PNUConfig pNUConfig) {
    PNUConfig = pNUConfig;
    wsConfigs.put(pNUConfig.getAcronym(), pNUConfig);
  }

  // public GEONAMESConfig getGEONAMESConfig() {
  // return GEONAMESConfig;
  // }
  //
  // public void setGEONAMESConfig(GEONAMESConfig gEONAMESConfig) {
  // GEONAMESConfig = gEONAMESConfig;
  // wsConfigs.put(gEONAMESConfig.getAcronym(), gEONAMESConfig);
  // }

  public PESIConfig getPESIConfig() {
    return PESIConfig;
  }

  public void setPESIConfig(PESIConfig pESIConfig) {
    PESIConfig = pESIConfig;
    wsConfigs.put(pESIConfig.getAcronym(), pESIConfig);
  }

  public WORMSConfig getWORMSConfig() {
    return WORMSConfig;
  }

  public void setWORMSConfig(WORMSConfig wORMSConfig) {
    WORMSConfig = wORMSConfig;
    wsConfigs.put(wORMSConfig.getAcronym(), wORMSConfig);
  }

  // public ITISConfig getITISConfig() {
  // return ITISConfig;
  // }
  //
  // public void setITISConfig(ITISConfig iTISConfig) {
  // ITISConfig = iTISConfig;
  // wsConfigs.put(iTISConfig.getAcronym(), iTISConfig);
  // }

  public DWBWSConfig getDWBWSConfig() {
    return DWBWSConfig;
  }

  public void setDWBWSConfig(DWBWSConfig dWBWSConfig) {
    DWBWSConfig = dWBWSConfig;
    wsConfigs.put(dWBWSConfig.getAcronym(), dWBWSConfig);
  }
}
