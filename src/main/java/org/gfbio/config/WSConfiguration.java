package org.gfbio.config;

import org.gfbio.interfaces.WSInterface.Domains;
import org.gfbio.interfaces.WSInterface.SearchModes;
import org.gfbio.interfaces.WSInterface.Services;

/**
 *
 * Base class for a webservice configuration
 *
 */
public abstract class WSConfiguration {

  /*
   * YAML properties
   */

  private String contact;
  private String name;
  private String keywords;
  private String description;
  private String storage = "external";
  private String contribution;
  private String contributors;
  private String languages;
  private String creators;
  private String acronym;
  private String webserviceURL;
  private String uri;
  private String version;

  // comma-separated string list of values
  private String domains;
  private String services;
  private String modes;

  /*
   * interal value arrays
   */

  private Services[] availableServices;
  private Domains[] wsDomains;
  private SearchModes[] searchModes;

  public Domains[] getWsDomains() {
    return wsDomains;
  }

  public SearchModes[] getSearchModes() {
    return searchModes;
  }

  public Services[] getAvailableServices() {
    return availableServices;
  }



  public WSConfiguration(String contact, String name, String keywords, String description,
      String storage, String contribution, String contributors, String languages, String creators,
      String acronym, String webserviceURL, String uri, String version) {
    super();
    this.contact = contact;
    this.name = name;
    this.keywords = keywords;
    this.description = description;
    this.storage = storage;
    this.contribution = contribution;
    this.contributors = contributors;
    this.languages = languages;
    this.creators = creators;
    this.acronym = acronym;
    this.webserviceURL = webserviceURL;
    this.uri = uri;
    this.version = version;
  }

  public WSConfiguration() {}

  public String getAcronym() {
    return acronym;
  }

  public void setAcronym(String acronym) {
    this.acronym = acronym;
  }

  public String getWebserviceURL() {
    return webserviceURL;
  }

  public void setWebserviceURL(String webserviceURL) {
    this.webserviceURL = webserviceURL;
  }

  public String getContact() {
    return contact;
  }

  public void setContact(String contact) {
    this.contact = contact;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getKeywords() {
    return keywords;
  }

  public void setKeywords(String keywords) {
    this.keywords = keywords;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getStorage() {
    return storage;
  }

  public void setStorage(String storage) {
    this.storage = storage;
  }

  public String getContribution() {
    return contribution;
  }

  public void setContribution(String contribution) {
    this.contribution = contribution;
  }

  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getServices() {
    return services;
  }

  public void setServices(String services) {
    this.services = services;
    String[] svcs = this.services.split(",");
    Services[] availableSvcs = new Services[svcs.length];

    for (int i = 0; i < svcs.length; i++) {
      String svc = svcs[i];
      availableSvcs[i] = Services.valueOf(svc.trim());
    }

    this.availableServices = availableSvcs;
  }

  public void setDomains(String domains) {
    this.domains = domains;
    String[] dmns = this.domains.split(",");
    Domains[] wsDomains = new Domains[dmns.length];

    for (int i = 0; i < dmns.length; i++) {
      String dmn = dmns[i];
      wsDomains[i] = Domains.valueOf(dmn.trim());
    }

    this.wsDomains = wsDomains;
  }

  public void setModes(String modes) {
    this.modes = modes;
    String[] mds = this.modes.split(",");
    SearchModes[] searchModes = new SearchModes[mds.length];

    for (int i = 0; i < mds.length; i++) {
      String md = mds[i];
      searchModes[i] = SearchModes.valueOf(md.trim());
    }

    this.searchModes = searchModes;
  }

  public String getContributors() {
    return contributors;
  }

  public void setContributors(String contributors) {
    this.contributors = contributors;
  }

  public String getLanguages() {
    return languages;
  }

  public void setLanguages(String languages) {
    this.languages = languages;
  }

  public String getCreators() {
    return creators;
  }

  public void setCreators(String creators) {
    this.creators = creators;
  }

}
