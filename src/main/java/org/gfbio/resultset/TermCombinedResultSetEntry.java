package org.gfbio.resultset;

import java.util.ArrayList;
import org.gfbio.formating.GFBioJsonArray;
import org.gfbio.formating.GFBioJsonString;
import org.gfbio.formating.GFBioResultSetEntry;
import org.gfbio.formating.GFBioResultSetEntry.GFBioJsonPair;
import org.gfbio.interfaces.GFBioRSEntry;

public class TermCombinedResultSetEntry
  implements GFBioRSEntry
{
  private GFBioResultSetEntry entry;
  private String label;
  private String uri;
  private String url;
  private String kingdom;
  private String rank;
  private String status;
  private String description;
  private ArrayList<String> synonyms;
  private ArrayList<String> commonNames;
  private ArrayList<String> abbreviations;
  private ArrayList<String> symbols;
  private String externalID;
  private String sourceTerminology;
  private String embeddedDatabase;
  private ArrayList<Pair<String, String>> termInfos;
  private ArrayList<Pair<String, ArrayList<String>>> arrayInfos;
  
  public TermCombinedResultSetEntry()
  {
    this.entry = new GFBioResultSetEntry();
    
    this.synonyms = new ArrayList();
    this.commonNames = new ArrayList();
    this.abbreviations = new ArrayList();
    this.symbols = new ArrayList();
    
    this.termInfos = new ArrayList();
    this.arrayInfos = new ArrayList();
  }
  
  private void create()
  {
    checkNullAndAdd("label", getLabel());
    checkNullAndAdd("uri", getUri());
    checkNullAndAdd("url", getUrl());
    checkNullAndAdd("kingdom", getKingdom());
    checkNullAndAdd("rank", getRank());
    checkNullAndAdd("status", getStatus());
    
    checkNullAndAdd("description", getDescription());
    checkNullAndAdd("synonyms", getSynonyms());
    checkNullAndAdd("commonNames", getCommonNames());
    checkNullAndAdd("abbreviations", getAbbreviations());
    checkNullAndAdd("symbols", getSymbols());
    
    checkNullAndAdd("externalID", getExternalID());
    checkNullAndAdd("sourceTerminology", getSourceTerminology());
    checkNullAndAdd("embeddedDatabase", getEmbeddedDatabase());
    for (Pair<String, String> p : this.termInfos) {
      checkNullAndAdd((String)p.getString(), (String)p.getValue());
    }
    for (Pair<String, ArrayList<String>> p : this.arrayInfos) {
      checkNullAndAdd((String)p.getString(), (ArrayList)p.getValue());
    }
  }
  
  private void checkNullAndAdd(String attr, String val)
  {
    if ((val != null) && (attr != null)) {
      this.entry.addJsonMember(attr, new GFBioJsonString(val));
    }
  }
  
  private void checkNullAndAdd(String attr, ArrayList<String> val)
  {
    if ((!val.isEmpty()) && (attr != null)) {
      this.entry.addJsonMember(attr, new GFBioJsonArray(val));
    }
  }
  
  public boolean isOriginal()
  {
    return false;
  }
  
  public GFBioResultSetEntry getEntry()
  {
    create();
    return this.entry;
  }
  
  public void setOriginalTermInfo(String attribute, String value)
  {
    this.termInfos.add(new Pair(attribute, value));
  }
  
  public void setOriginalTermInfo(String attribute, ArrayList<String> value)
  {
    this.arrayInfos.add(new Pair(attribute, value));
  }
  
  private String getLabel()
  {
    return this.label;
  }
  
  public void setLabel(String label)
  {
    this.label = label;
  }
  
  private String getUri()
  {
    return this.uri;
  }
  
  public void setUri(String uri)
  {
    this.uri = uri;
  }
  
  private String getUrl()
  {
    return this.url;
  }
  
  public void setUrl(String url)
  {
    this.url = url;
  }
  
  private String getKingdom()
  {
    return this.kingdom;
  }
  
  public void setKingdom(String kingdom)
  {
    this.kingdom = kingdom;
  }
  
  private String getRank()
  {
    return this.rank;
  }
  
  public void setRank(String rank)
  {
    this.rank = rank;
  }
  
  private String getStatus()
  {
    return this.status;
  }
  
  public void setStatus(String status)
  {
    this.status = status;
  }
  
  private String getDescription()
  {
    return this.description;
  }
  
  public void setDescription(String description)
  {
    this.description = description;
  }
  
  private ArrayList<String> getSynonyms()
  {
    return this.synonyms;
  }
  
  public void setSynonym(String synonym)
  {
    this.synonyms.add(synonym);
  }
  
  public void setSynonyms(ArrayList<String> synonyms)
  {
    this.synonyms.addAll(synonyms);
  }
  
  private ArrayList<String> getCommonNames()
  {
    return this.commonNames;
  }
  
  public void setCommonName(String commonName)
  {
    if (!commonName.equals("")) {
      this.commonNames.add(commonName);
    }
  }
  
  public void setCommonNames(ArrayList<String> commonNames)
  {
    this.commonNames.addAll(commonNames);
  }
  
  public ArrayList<String> getAbbreviations()
  {
    return this.abbreviations;
  }
  
  public void setAbbreviation(String abbreviation)
  {
    this.abbreviations.add(abbreviation);
  }
  
  public void setAbbreviations(ArrayList<String> abbreviations)
  {
    this.abbreviations.addAll(abbreviations);
  }
  
  private ArrayList<String> getSymbols()
  {
    return this.symbols;
  }
  
  public void setSymbol(String symbol)
  {
    this.symbols.add(symbol);
  }
  
  public void setSymbols(ArrayList<String> symbols)
  {
    this.symbols.addAll(symbols);
  }
  
  private String getExternalID()
  {
    return this.externalID;
  }
  
  public void setExternalID(String externalID)
  {
    this.externalID = externalID;
  }
  
  private String getSourceTerminology()
  {
    return this.sourceTerminology;
  }
  
  public void setSourceTerminology(String sourceTerminology)
  {
    this.sourceTerminology = sourceTerminology;
  }
  
  private String getEmbeddedDatabase()
  {
    return this.embeddedDatabase;
  }
  
  public void setEmbeddedDatabase(String embeddedDatabase)
  {
    this.embeddedDatabase = embeddedDatabase;
  }
  
  public class Pair<L, R>
  {
    private final L string;
    private final R value;
    
    public Pair(L string, R value)
    {
      this.string = string;
      this.value = value;
    }
    
    public L getString()
    {
      return (L)this.string;
    }
    
    public R getValue()
    {
      return (R)this.value;
    }
    
    public int hashCode()
    {
      return this.string.hashCode() ^ this.value.hashCode();
    }
    
    public boolean equals(Object o)
    {
      if (!(o instanceof GFBioResultSetEntry.GFBioJsonPair)) {
        return false;
      }
      Pair pairo = (Pair)o;
      return (this.string.equals(pairo.getString())) && (this.value.equals(pairo.getValue()));
    }
  }
}
