package org.gfbio.resultset;

import java.util.ArrayList;
import org.gfbio.formating.GFBioJsonArray;
import org.gfbio.formating.GFBioJsonString;
import org.gfbio.formating.GFBioResultSetEntry;
import org.gfbio.interfaces.GFBioRSEntry;

public class HierarchyResultSetEntry
  implements GFBioRSEntry
{
  private GFBioResultSetEntry entry = new GFBioResultSetEntry();
  private ArrayList<String> hierarchy = new ArrayList();
  private String label;
  private String uri;
  private String rank;
  private String externalID;
  
  private void create()
  {
    checkNullAndAdd("label", getLabel());
    checkNullAndAdd("rank", getRank());
    checkNullAndAdd("uri", getUri());
    checkNullAndAdd("externalID", getExternalID());
    this.entry.addJsonMember("hierarchy", new GFBioJsonArray(getHierarchy()));
  }
  
  private void checkNullAndAdd(String attr, String val)
  {
    if ((val != null) && (attr != null)) {
      this.entry.addJsonMember(attr, new GFBioJsonString(val));
    }
  }
  
  public GFBioResultSetEntry getEntry()
  {
    create();
    return this.entry;
  }
  
  public boolean isOriginal()
  {
    return false;
  }
  
  private ArrayList<String> getHierarchy()
  {
    return this.hierarchy;
  }
  
  public void setHierarchy(ArrayList<String> hierarchy)
  {
    this.hierarchy.addAll(hierarchy);
  }
  
  private String getLabel()
  {
    return this.label;
  }
  
  public void setLabel(String label)
  {
    this.label = label;
  }
  
  private String getUri()
  {
    return this.uri;
  }
  
  public void setUri(String uri)
  {
    this.uri = uri;
  }
  
  private String getRank()
  {
    return this.rank;
  }
  
  public void setRank(String rank)
  {
    this.rank = rank;
  }
  
  private String getExternalID()
  {
    return this.externalID;
  }
  
  public void setExternalID(String externalID)
  {
    this.externalID = externalID;
  }
}
