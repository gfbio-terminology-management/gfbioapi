package org.gfbio.resultset;

import java.util.ArrayList;
import javax.json.JsonObject;
import org.gfbio.formating.GFBioTSFormatter;
import org.gfbio.interfaces.GFBioRSEntry;

public class GFBioResultSet<T extends GFBioRSEntry> {
  private GFBioTSFormatter formatter = new GFBioTSFormatter();
  private String webservice;

  public GFBioResultSet(String webservice) {
    this.webservice = webservice;
  }

  public void addEntry(T entry) {
    this.formatter.addEntry(entry.getEntry());
    this.formatter.setOriginal(entry.isOriginal());
  }

  public void addWarning(String message) {
    this.formatter.createWarning(message);
  }

  public void addError(String message) {
    this.formatter.createError(message);
  }

  public ArrayList<JsonObject> create() {
    return this.formatter.getResultsAsJson(this.webservice);
  }

  public int numEntries() {
    return formatter.numResults();
  }
}
