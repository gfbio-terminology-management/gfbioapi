
package org.gfbio.resultset;

import java.util.ArrayList;

import org.gfbio.formating.GFBioJsonArray;
import org.gfbio.formating.GFBioResultSetEntry;
import org.gfbio.interfaces.GFBioRSEntry;

public class CapabilitiesResultSetEntry
  implements GFBioRSEntry
{
  private GFBioResultSetEntry entry = new GFBioResultSetEntry();
  private ArrayList<String> availableServices = new ArrayList<String>();
  private ArrayList<String> searchModes = new ArrayList<String>();

  private void create()
  {
    checkNullAndAdd("availableServices", getAvailableServices());
    checkNullAndAdd("searchModes", getSearchModes());
  }


  private void checkNullAndAdd(String attr, ArrayList<String> val)
  {
    if ((attr != null) && (!val.isEmpty())) {
      this.entry.addJsonMember(attr, new GFBioJsonArray(val));
    }
  }


  public GFBioResultSetEntry getEntry()
  {
    create();
    return this.entry;
  }

  public boolean isOriginal()
  {
    return false;
  }

  public void setAvailableServices(ArrayList<String> services)
  {
    this.availableServices.addAll(services);
  }

  public ArrayList<String> getAvailableServices() {
        return availableServices;
  }


  public void setSearchModes(ArrayList<String> modes)
  {
    this.searchModes.addAll(modes);
  }

  public ArrayList<String> getSearchModes() {
        return searchModes;
  }

public class Pair<L, R>
  {
    private final L string;
    private final R value;

    public Pair(L string, R value)
    {
      this.string = string;
      this.value = value;
    }

    public L getString()
    {
      return (L)this.string;
    }

    public R getValue()
    {
      return (R)this.value;
    }

    public int hashCode()
    {
      return this.string.hashCode() ^ this.value.hashCode();
    }

    public boolean equals(Object o)
    {
      if (!(o instanceof GFBioResultSetEntry.GFBioJsonPair)) {
        return false;
      }
      Pair pairo = (Pair)o;
      return (this.string.equals(pairo.getString())) && (this.value.equals(pairo.getValue()));
    }
  }
}

