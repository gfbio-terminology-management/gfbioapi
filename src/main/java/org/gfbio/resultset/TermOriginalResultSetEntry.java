package org.gfbio.resultset;

import java.util.ArrayList;
import org.gfbio.formating.GFBioJsonArray;
import org.gfbio.formating.GFBioJsonString;
import org.gfbio.formating.GFBioResultSetEntry;
import org.gfbio.formating.GFBioResultSetEntry.GFBioJsonPair;
import org.gfbio.interfaces.GFBioRSEntry;

public class TermOriginalResultSetEntry
  implements GFBioRSEntry
{
  private GFBioResultSetEntry entry;
  private ArrayList<Pair<String, String>> termInfos;
  private ArrayList<Pair<String, ArrayList<String>>> arrayInfos;
  
  public TermOriginalResultSetEntry()
  {
    this.entry = new GFBioResultSetEntry();
    
    this.termInfos = new ArrayList();
    this.arrayInfos = new ArrayList();
  }
  
  private void create()
  {
    for (Pair<String, String> p : this.termInfos) {
      checkNullAndAdd((String)p.getString(), (String)p.getValue());
    }
    for (Pair<String, ArrayList<String>> p : this.arrayInfos) {
      checkNullAndAdd((String)p.getString(), (ArrayList)p.getValue());
    }
  }
  
  private void checkNullAndAdd(String attr, String val)
  {
    if ((val != null) && (attr != null)) {
      this.entry.addJsonMember(attr, new GFBioJsonString(val));
    }
  }
  
  private void checkNullAndAdd(String attr, ArrayList<String> val)
  {
    if ((!val.isEmpty()) && (attr != null)) {
      this.entry.addJsonMember(attr, new GFBioJsonArray(val));
    }
  }
  
  public boolean isOriginal()
  {
    return true;
  }
  
  public GFBioResultSetEntry getEntry()
  {
    create();
    return this.entry;
  }
  
  public void setOriginalTermInfo(String attribute, String value)
  {
    this.termInfos.add(new Pair(attribute, value));
  }
  
  public void setOriginalTermInfo(String attribute, ArrayList<String> value)
  {
    this.arrayInfos.add(new Pair(attribute, value));
  }
  
  public class Pair<L, R>
  {
    private final L string;
    private final R value;
    
    public Pair(L string, R value)
    {
      this.string = string;
      this.value = value;
    }
    
    public L getString()
    {
      return (L)this.string;
    }
    
    public R getValue()
    {
      return (R)this.value;
    }
    
    public int hashCode()
    {
      return this.string.hashCode() ^ this.value.hashCode();
    }
    
    public boolean equals(Object o)
    {
      if (!(o instanceof GFBioResultSetEntry.GFBioJsonPair)) {
        return false;
      }
      Pair pairo = (Pair)o;
      return (this.string.equals(pairo.getString())) && (this.value.equals(pairo.getValue()));
    }
  }
}
