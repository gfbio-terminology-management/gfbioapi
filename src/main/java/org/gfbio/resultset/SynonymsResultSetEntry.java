package org.gfbio.resultset;

import java.util.ArrayList;
import org.gfbio.formating.GFBioJsonArray;
import org.gfbio.formating.GFBioResultSetEntry;
import org.gfbio.interfaces.GFBioRSEntry;

public class SynonymsResultSetEntry
  implements GFBioRSEntry
{
  private GFBioResultSetEntry entry = new GFBioResultSetEntry();
  private ArrayList<String> synonyms;
  
  private void create()
  {
    checkNullAndAdd("synonyms", getSynonyms());
  }
  
  public GFBioResultSetEntry getEntry()
  {
    create();
    return this.entry;
  }
  
  private void checkNullAndAdd(String attr, ArrayList<String> val)
  {
    if ((val != null) && (attr != null)) {
      this.entry.addJsonMember(attr, new GFBioJsonArray(val));
    }
  }
  
  public boolean isOriginal()
  {
    return false;
  }
  
  private ArrayList<String> getSynonyms()
  {
    return this.synonyms;
  }
  
  public void setSynonyms(ArrayList<String> synonyms)
  {
    this.synonyms = synonyms;
  }
}
