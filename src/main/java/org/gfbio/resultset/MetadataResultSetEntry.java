package org.gfbio.resultset;

import java.util.ArrayList;
import org.gfbio.formating.GFBioJsonArray;
import org.gfbio.formating.GFBioJsonString;
import org.gfbio.formating.GFBioResultSetEntry;
import org.gfbio.interfaces.GFBioRSEntry;

public class MetadataResultSetEntry
  implements GFBioRSEntry
{
  private GFBioResultSetEntry entry = new GFBioResultSetEntry();
  private String name;
  private String acronym;
  private String version;
  private String description;
  private String keywords;
  private String uri;
  private String contact;
  private String language;
  private String contributors;
  private String creators;
  private String storage;
  private String contribution;
  private ArrayList<String> namespaces = new ArrayList();
  private ArrayList<String> domain = new ArrayList();
  private ArrayList<String> embeddedDatabase = new ArrayList();
  private ArrayList<Pair<String, String>> termInfos = new ArrayList();
  private ArrayList<Pair<String, ArrayList<String>>> arrayInfos = new ArrayList();
  
  private void create()
  {
    checkNullAndAdd("serviceName", getName());
    checkNullAndAdd("serviceAcronym", getAcronym());
    checkNullAndAdd("serviceVersion", getVersion());
    checkNullAndAdd("serviceDescription", getDescription());
    checkNullAndAdd("serviceKeywords", getKeywords());
    checkNullAndAdd("serviceURI", getUri());
    checkNullAndAdd("serviceContact", getContact());
    checkNullAndAdd("serviceDomains", getDomain());
    checkNullAndAdd("serviceLanguage", getLanguage());
    checkNullAndAdd("serviceCreators", getCreators());
    checkNullAndAdd("serviceContributors", getContributors());
    checkNullAndAdd("embeddedDatabases", getEmbeddedDatabase());
    checkNullAndAdd("storage", getStorage());
    checkNullAndAdd("contribution", getContribution());
    checkNullAndAdd("namespaces", getNamespace());
    for (Pair<String, String> p : this.termInfos) {
        checkNullAndAdd((String)p.getString(), (String)p.getValue());
      }
      for (Pair<String, ArrayList<String>> p : this.arrayInfos) {
        checkNullAndAdd((String)p.getString(), (ArrayList)p.getValue());
      }
  }
  
  private void checkNullAndAdd(String attr, String val)
  {
    if ((attr != null) && (val != null)) {
      this.entry.addJsonMember(attr, new GFBioJsonString(val));
    }
  }
  
  private void checkNullAndAdd(String attr, ArrayList<String> val)
  {
    if ((attr != null) && (!val.isEmpty())) {
      this.entry.addJsonMember(attr, new GFBioJsonArray(val));
    }
  }
  public void setOriginalTermInfo(String attribute, String value)
  {
    this.termInfos.add(new Pair(attribute, value));
  }
  
  public void setOriginalTermInfo(String attribute, ArrayList<String> value)
  {
    this.arrayInfos.add(new Pair(attribute, value));
  }
  public GFBioResultSetEntry getEntry()
  {
    create();
    return this.entry;
  }
  
  public boolean isOriginal()
  {
    return false;
  }
  
  private String getName()
  {
    return this.name;
  }
  
  public void setName(String name)
  {
    this.name = name;
  }
  
  private String getAcronym()
  {
    return this.acronym;
  }
  
  public void setAcronym(String acronym)
  {
    this.acronym = acronym;
  }
  
  private String getVersion()
  {
    return this.version;
  }
  
  public void setVersion(String version)
  {
    this.version = version;
  }
  
  private String getDescription()
  {
    return this.description;
  }
  
  public void setDescription(String description)
  {
    this.description = description;
  }
  
  private String getKeywords()
  {
    return this.keywords;
  }
  
  public void setKeywords(String keywords)
  {
    this.keywords = keywords;
  }
  
  private String getUri()
  {
    return this.uri;
  }
  
  public void setUri(String uri)
  {
    this.uri = uri;
  }
  
  private String getContact()
  {
    return this.contact;
  }
  
  public void setContact(String contact)
  {
    this.contact = contact;
  }
  
  private ArrayList<String> getDomain()
  {
    return this.domain;
  }
  
  public void setDomain(String domain)
  {
    this.domain.add(domain);
  }
  
  public void setDomain(ArrayList<String> domain)
  {
    this.domain.addAll(domain);
  }
  public String getLanguage() {
	return language;
}

public void setLanguage(String language) {
	this.language = language;
}
public String getContributors() {
	return contributors;
}

public void setContributors(String contributors) {
	this.contributors = contributors;
}
public String getCreators() {
	return creators;
}

public void setCreators(String creators) {
	this.creators = creators;
}
public ArrayList<String> getEmbeddedDatabase() {
	return embeddedDatabase;
}

public void setEmbeddedDatabase(ArrayList<String> embeddedDatabase) {
	this.embeddedDatabase.addAll(embeddedDatabase);
}
public void setEmbeddedDatabase(String embeddedDatabase) {
	this.embeddedDatabase.add(embeddedDatabase);
}
public String getStorage() {
	return storage;
}

public void setStorage(String storage) {
	this.storage = storage;
}
public String getContribution() {
	return contribution;
}

public void setContribution(String contribution) {
	this.contribution = contribution;
}
public ArrayList<String> getNamespace() {
	return namespaces;
}

public void setNamespaces( ArrayList<String> namespaces) {
	this.namespaces = namespaces;
}
public class Pair<L, R>
  {
    private final L string;
    private final R value;
    
    public Pair(L string, R value)
    {
      this.string = string;
      this.value = value;
    }
    
    public L getString()
    {
      return (L)this.string;
    }
    
    public R getValue()
    {
      return (R)this.value;
    }
    
    public int hashCode()
    {
      return this.string.hashCode() ^ this.value.hashCode();
    }
    
    public boolean equals(Object o)
    {
      if (!(o instanceof GFBioResultSetEntry.GFBioJsonPair)) {
        return false;
      }
      Pair pairo = (Pair)o;
      return (this.string.equals(pairo.getString())) && (this.value.equals(pairo.getValue()));
    }
  }
}
