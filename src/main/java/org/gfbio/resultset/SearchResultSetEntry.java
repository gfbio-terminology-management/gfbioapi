package org.gfbio.resultset;

import java.util.ArrayList;
import org.gfbio.formating.GFBioJsonArray;
import org.gfbio.formating.GFBioJsonString;
import org.gfbio.formating.GFBioResultSetEntry;
import org.gfbio.interfaces.GFBioRSEntry;

public class SearchResultSetEntry implements GFBioRSEntry {
  private GFBioResultSetEntry entry = new GFBioResultSetEntry();
  private String label;
  private String uri;
  private String url;
  private String kingdom;
  private String rank;
  private String status;
  private String description;
  private ArrayList<String> synonyms = new ArrayList<String>();
  private ArrayList<String> commonNames = new ArrayList<String>();
  private ArrayList<String> abbreviations = new ArrayList<String>();
  private ArrayList<String> symbols = new ArrayList<String>();
  private String externalID;
  private String sourceTerminology;
  private String embeddedDatabase;
  private String internal;

  private void create() {
    checkNullAndAdd("label", getLabel());
    checkNullAndAdd("uri", getUri());
    checkNullAndAdd("url", getUrl());
    checkNullAndAdd("kingdom", getKingdom());
    checkNullAndAdd("rank", getRank());
    checkNullAndAdd("status", getStatus());

    checkNullAndAdd("description", getDescription());
    checkNullAndAdd("synonyms", getSynonyms());
    checkNullAndAdd("commonNames", getCommonNames());
    checkNullAndAdd("abbreviations", getAbbreviations());
    checkNullAndAdd("symbols", getSymbols());

    checkNullAndAdd("externalID", getExternalID());
    checkNullAndAdd("sourceTerminology", getSourceTerminology());
    checkNullAndAdd("embeddedDatabase", getEmbeddedDatabase());
    checkNullAndAdd("internal", getInternal());

  }

  private void checkNullAndAdd(String attr, String val) {
    if ((attr != null) && (val != null)) {
      this.entry.addJsonMember(attr, new GFBioJsonString(val));
    }
  }

  private void checkNullAndAdd(String attr, ArrayList<String> val) {
    if ((attr != null) && (!val.isEmpty())) {
      this.entry.addJsonMember(attr, new GFBioJsonArray(val));
    }
  }

  public boolean isOriginal() {
    return false;
  }

  public GFBioResultSetEntry getEntry() {
    create();
    return this.entry;
  }

  private String getLabel() {
    return this.label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  private String getUri() {
    return this.uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

  private String getUrl() {
    return this.url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  private String getKingdom() {
    return this.kingdom;
  }

  public void setKingdom(String kingdom) {
    this.kingdom = kingdom;
  }

  private String getRank() {
    return this.rank;
  }

  public void setRank(String rank) {
    this.rank = rank;
  }

  private String getStatus() {
    return this.status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  private String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  private ArrayList<String> getSynonyms() {
    return this.synonyms;
  }

  public void setSynonym(String synonym) {
    this.synonyms.add(synonym);
  }

  public void setSynonyms(ArrayList<String> synonyms) {
    this.synonyms.addAll(synonyms);
  }

  private ArrayList<String> getCommonNames() {
    return this.commonNames;
  }

  public void setCommonName(String commonName) {
    if (!commonName.equals("")) {
      this.commonNames.add(commonName);
    }
  }

  public void setCommonNames(ArrayList<String> commonNames) {
    this.commonNames.addAll(commonNames);
  }

  public ArrayList<String> getAbbreviations() {
    return this.abbreviations;
  }

  public void setAbbreviation(String abbreviation) {
    this.abbreviations.add(abbreviation);
  }

  public void setAbbreviations(ArrayList<String> abbreviations) {
    this.abbreviations.addAll(abbreviations);
  }

  private ArrayList<String> getSymbols() {
    return this.symbols;
  }

  public void setSymbol(String symbol) {
    this.symbols.add(symbol);
  }

  public void setSymbols(ArrayList<String> symbols) {
    this.symbols.addAll(symbols);
  }

  private String getExternalID() {
    return this.externalID;
  }

  public void setExternalID(String externalID) {
    this.externalID = externalID;
  }

  private String getSourceTerminology() {
    return this.sourceTerminology;
  }

  public void setSourceTerminology(String sourceTerminology) {
    this.sourceTerminology = sourceTerminology;
  }

  private String getEmbeddedDatabase() {
    return this.embeddedDatabase;
  }

  public void setEmbeddedDatabase(String embeddedDatabase) {
    this.embeddedDatabase = embeddedDatabase;
  }

  public String getInternal() {
    return internal;
  }

  public void setInternal(String internal) {
    this.internal = internal;
  }
}
