package org.gfbio.formating;

import java.util.ArrayList;

public class GFBioResultSetEntry
{
  ArrayList<GFBioJsonPair> attributes;
  
  public GFBioResultSetEntry()
  {
    this.attributes = new ArrayList();
  }
  
  public void addJsonMember(String key, GFBioJsonString value)
  {
    this.attributes.add(new GFBioJsonPair(key, value));
  }
  
  public void addJsonMember(String key, GFBioJsonArray value)
  {
    this.attributes.add(new GFBioJsonPair(key, value));
  }
  
  public GFBioJsonPair getNextMember()
  {
    return (GFBioJsonPair)this.attributes.remove(0);
  }
  
  public boolean isEmpty()
  {
    if (this.attributes.isEmpty()) {
      return true;
    }
    return false;
  }
  
  public void clear()
  {
    this.attributes.clear();
  }
  
  public class GFBioJsonPair<L, R>
  {
    private final L string;
    private final R value;
    
    public GFBioJsonPair(L string, R value)
    {
      this.string = string;
      this.value = value;
    }
    
    public L getString()
    {
      return (L)this.string;
    }
    
    public R getValue()
    {
      return (R)this.value;
    }
    
    public int hashCode()
    {
      return this.string.hashCode() ^ this.value.hashCode();
    }
    
    public boolean equals(Object o)
    {
      if (!(o instanceof GFBioJsonPair)) {
        return false;
      }
      GFBioJsonPair pairo = (GFBioJsonPair)o;
      return (this.string.equals(pairo.getString())) && (this.value.equals(pairo.getValue()));
    }
  }
}
