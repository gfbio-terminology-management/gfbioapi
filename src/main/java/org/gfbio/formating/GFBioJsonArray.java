package org.gfbio.formating;

import java.util.ArrayList;
import java.util.List;

import org.gfbio.interfaces.GFBioJsonElement;

public class GFBioJsonArray
  implements GFBioJsonElement
{
  private ArrayList<String> elements = new ArrayList<String>();
  
  public GFBioJsonArray() {}
  
  public GFBioJsonArray(ArrayList<String> array)
  {
    this.elements.addAll(array);
  }
  
  public void addElement(String e)
  {
    this.elements.add(e);
  }
  
  public void addElements(List<String> e)
  {
    this.elements.addAll(e);
  }
  
  public String getNextElement()
  {
    return (String)this.elements.remove(0);
  }
  
  public boolean isArray()
  {
    return true;
  }
  
  public boolean hasNext()
  {
    if (!this.elements.isEmpty()) {
      return true;
    }
    return false;
  }
}
