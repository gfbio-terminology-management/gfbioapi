package org.gfbio.formating;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import org.apache.log4j.Logger;
import org.gfbio.interfaces.GFBioJsonElement;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;

/**
 * 
 * @apiNote This depends on a running Virtuoso instance, which contains all necessary GFBio-related
 *          schemes
 *
 */
public class GFBioTSFormatter {
  private static final Logger LOGGER = Logger.getLogger(GFBioTSFormatter.class);
  HashMap<String, String> attributeMap = new HashMap<String, String>();
  HashMap<String, String> contextMap = new HashMap<String, String>();
  private ArrayList<GFBioResultSetEntry> entryList = new ArrayList<GFBioResultSetEntry>();
  private String serverUrl = "http://localhost:8890";
  private boolean original;
  private ArrayList<JsonObject> resultList = new ArrayList<JsonObject>();

  public void addEntry(GFBioResultSetEntry entry) {
    this.entryList.add(entry);
  }

  public void setOriginal(boolean original) {
    this.original = original;
  }

  public ArrayList<JsonObject> getResultsAsJson(String webserviceName) {
    String baseUri = "http://terminologies.gfbio.org/terms/" + webserviceName + "-schema/";
    String tsUri = "http://terminologies.gfbio.org/terms/ts-schema/";
    String metaUri = "http://terminologies.gfbio.org/terms/metadata-schema/";
    if ((this.entryList.isEmpty()) && (this.resultList.isEmpty())) {
      return resultList;
    }
    for (GFBioResultSetEntry entry : this.entryList) {
      JsonObjectBuilder value = Json.createObjectBuilder();
      while (!entry.isEmpty()) {
        GFBioResultSetEntry.GFBioJsonPair<String, GFBioJsonElement> pair = entry.getNextMember();
        if (!this.attributeMap.containsKey(pair.getString())) {
          StringBuilder querybuilder = new StringBuilder();
          if (!this.original) {
            querybuilder.append("PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ");
            querybuilder
                .append("select ?gfbiotsLabel ?externalLabel ?externalName ?externalURI where ");
            querybuilder
                .append("{OPTIONAL{<" + tsUri + (String) pair.getString() + "> rdfs:label ");
            querybuilder.append("?gfbiotsLabel} ");
            querybuilder.append(".OPTIONAL { <" + baseUri + (String) pair.getString()
                + "> rdfs:label ?externalLabel}");
            querybuilder.append(".OPTIONAL{ <" + metaUri + (String) pair.getString()
                + "> rdfs:subPropertyOf ?externalURI . ?externalURI rdfs:label ?externalName}}");
          } else {
            querybuilder.append("PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ");
            querybuilder.append("select ?externalLabel ?externalURI where ");
            querybuilder.append(
                "{ <" + baseUri + (String) pair.getString() + "> rdfs:label ?externalLabel}");
          }
          Query query = QueryFactory.create(querybuilder.toString());
          QueryExecution qExe =
              QueryExecutionFactory.sparqlService(this.serverUrl + "/sparql", query);
          LOGGER.info("executing" + query);

          ResultSet results = qExe.execSelect();
          if (results.hasNext()) {
            QuerySolution atttributeSolution = results.next();
            if (atttributeSolution.get("gfbiotsLabel") != null) {
              this.attributeMap.put(pair.getString(),
                  ((Literal) atttributeSolution.get("gfbiotsLabel").as(Literal.class))
                      .getLexicalForm().toString());
              this.contextMap
                  .put(((Literal) atttributeSolution.get("gfbiotsLabel").as(Literal.class))
                      .getLexicalForm().toString(), tsUri + (String) pair.getString());
            } else if (atttributeSolution.get("externalLabel") != null) {
              this.attributeMap.put(pair.getString(),
                  atttributeSolution.get("externalLabel").toString());
              this.contextMap.put(atttributeSolution.get("externalLabel").toString(),
                  baseUri + (String) pair.getString());
            } else if (atttributeSolution.get("externalName") != null) {
              this.attributeMap.put(pair.getString(),
                  ((Literal) atttributeSolution.get("externalName").as(Literal.class))
                      .getLexicalForm().toString());
              this.contextMap.put(
                  ((Literal) atttributeSolution.get("externalName").as(Literal.class))
                      .getLexicalForm().toString(),
                  atttributeSolution.get("externalURI").toString());
            }
          }
        }
        if (this.attributeMap.containsKey(pair.getString())) {
          if (((GFBioJsonElement) pair.getValue()).isArray()) {
            JsonArrayBuilder array = Json.createArrayBuilder();
            GFBioJsonArray elem = (GFBioJsonArray) pair.getValue();
            while (elem.hasNext()) {
              array.add(elem.getNextElement());
            }
            value.add((String) this.attributeMap.get(pair.getString()), array.build());
          } else {
            value.add((String) this.attributeMap.get(pair.getString()),
                ((GFBioJsonString) pair.getValue()).getJsonString());
          }
        }
      }
      this.resultList.add(value.build());
    }
    putContext(this.resultList);
    return this.resultList;
  }

  private void putContext(ArrayList<JsonObject> resultList) {
    JsonObjectBuilder value = Json.createObjectBuilder();
    Iterator it = this.contextMap.entrySet().iterator();
    while (it.hasNext()) {
      Map.Entry pair = (Map.Entry) it.next();
      value.add(pair.getKey().toString(), pair.getValue().toString());
      it.remove();
    }
    JsonObjectBuilder b = Json.createObjectBuilder().add("context", value.build());

    resultList.add(b.build());
  }

  public void createWarning(String message) {
    JsonObjectBuilder value = Json.createObjectBuilder();
    value.add("warning", message);
    this.resultList.add(value.build());
  }

  public void createError(String message) {
    JsonObjectBuilder value = Json.createObjectBuilder();
    value.add("error", message);
    this.resultList.add(value.build());
  }

  public int numResults() {
    return this.entryList.size();
  }
}
