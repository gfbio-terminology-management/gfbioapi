package org.gfbio.formating;

import org.gfbio.interfaces.GFBioJsonElement;

public class GFBioJsonString
  implements GFBioJsonElement
{
  private String element = "";
  
  public GFBioJsonString() {}
  
  public GFBioJsonString(String string)
  {
    this.element = string;
  }
  
  public void setJsonString(String string)
  {
    this.element = string;
  }
  
  public String getJsonString()
  {
    return this.element;
  }
  
  public boolean isArray()
  {
    return false;
  }
}
