package org.gfbio.db;

import org.gfbio.config.ConfigSvc;

public class VirtuosoSettings {

  private ConfigSvc cfgSvc;

  public VirtuosoSettings() {
    cfgSvc = ConfigSvc.getInstance(null);
  }

  public String getDBPassword() {
    return cfgSvc.getSettingFromINI("VIRTUOSO", "password");
  }

  public String getDBUserName() {
    return cfgSvc.getSettingFromINI("VIRTUOSO", "username");
  }

  public String getHostURL() {
    return cfgSvc.getSettingFromINI("VIRTUOSO", "virtuosoURL");
  }
}
