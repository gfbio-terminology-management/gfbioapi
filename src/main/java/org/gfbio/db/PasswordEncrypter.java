package org.gfbio.db;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class PasswordEncrypter {
	private static final String PRIVATE_KEY = "gfbio";
	
	public static String decryptPasswd(String encrypted_password){
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setPassword(PRIVATE_KEY);
		encryptor.initialize();
		return encryptor.decrypt(encrypted_password);
	}
	
	public static String enctyptPasswd(String plain_password){
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		
		encryptor.setPassword(PRIVATE_KEY);
		encryptor.initialize();
		return encryptor.encrypt(plain_password);
	}
}