package org.gfbio.db;

import virtuoso.jena.driver.VirtGraph;

public class VirtGraphSingleton {

  private static VirtGraphSingleton instance = null;
  VirtuosoSettings settings = new VirtuosoSettings();
  private String hostURL = settings.getHostURL();
  private String userName = settings.getDBUserName();
  private String password = settings.getDBPassword();
  private VirtGraph set = new VirtGraph(hostURL, userName, password);

  protected VirtGraphSingleton() {}

  public static VirtGraphSingleton getInstance() {
    if (instance == null) {
      instance = new VirtGraphSingleton();
    }

    return instance;
  }

  public VirtGraph getVirtGraph() {
    return set;
  }
}
